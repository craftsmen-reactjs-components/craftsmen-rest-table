function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var reactstrap = require('reactstrap');
var reactTable = require('react-table');

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var TableHandlers = function TableHandlers() {
  var _this = this;

  this.defaultDummyHandler = function () {
    console.warn("No TableHandler");
  };

  this.viewActionHandler = this.defaultDummyHandler;
  this.editActionHandler = this.defaultDummyHandler;
  this.deleteActionHandler = this.defaultDummyHandler;
  this.tableRowClickHandler = this.defaultDummyHandler;

  this.putTableHandlers = function (tableHandlers) {
    for (var key in tableHandlers) {
      _this[key] = tableHandlers[key];
    }
  };
};

var TableOptions = /*#__PURE__*/function () {
  function TableOptions() {
    var _this = this;

    this.showViewAction = false;
    this.showEditAction = false;
    this.showDeleteAction = false;
    this.clickableTable = true;
    this.columnSorting = false;

    this.putTableOptions = function (tableOptions) {
      for (var key in tableOptions) {
        _this[key] = tableOptions[key];
      }
    };
  }

  _createClass(TableOptions, [{
    key: "showActions",
    get: function get() {
      return this.showViewAction || this.showEditAction || this.showDeleteAction;
    }
  }]);

  return TableOptions;
}();

function ReactTableBasicHeaderRenderer(_ref) {
  var headerGroups = _ref.headerGroups,
      tableOptions = _ref.tableOptions;
  var actionHeaders = null;

  if (tableOptions.showActions) {
    actionHeaders = /*#__PURE__*/React__default.createElement("th", {
      key: "action-buttons-header"
    }, "Actions");
  }

  var columnMaper = function columnMaper(column) {
    return /*#__PURE__*/React__default.createElement("th", column.getHeaderProps(column.getSortByToggleProps), column.render("Header"), /*#__PURE__*/React__default.createElement("span", null, column.isSorted ? column.isSortedDesc ? " 🔽" : " 🔼" : ""));
  };

  var headerGroupsMaper = function headerGroupsMaper(headerGroup) {
    var columns = headerGroup.headers.map(columnMaper);
    columns.push(actionHeaders);
    return /*#__PURE__*/React__default.createElement("tr", headerGroup.getHeaderGroupProps(), columns);
  };

  return /*#__PURE__*/React__default.createElement("thead", null, headerGroups.map(headerGroupsMaper));
}

function TableActionButton(_ref) {
  var id = _ref.id,
      actionText = _ref.actionText,
      color = _ref.color,
      actionHandler = _ref.actionHandler,
      actionIcon = _ref.actionIcon;
  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement(reactstrap.UncontrolledTooltip, {
    placement: "top",
    target: id
  }, actionText), /*#__PURE__*/React__default.createElement(reactstrap.Button, {
    id: id,
    outline: true,
    color: color,
    size: "sm",
    onClick: actionHandler,
    className: "btn-icon btn-link btn-round"
  }, /*#__PURE__*/React__default.createElement("i", {
    className: actionIcon
  })));
}

function TableActionButtons(props) {
  var actionButtons = [];
  var index = props.index;

  if (props.tableOptions.showViewAction) {
    actionButtons.push( /*#__PURE__*/React__default.createElement(TableActionButton, {
      key: "view-action",
      id: "viewAction-" + index,
      color: "info",
      actionHandler: function actionHandler() {
        props.tableHandlers.viewActionHandler(props.item, props.index, props.tableModel);
      },
      actionIcon: "nc-icon nc-alert-circle-i",
      actionText: "View"
    }));
  }

  if (props.tableOptions.showEditAction) {
    actionButtons.push( /*#__PURE__*/React__default.createElement(TableActionButton, {
      key: "edit-action",
      id: "editAction-" + index,
      color: "primary",
      actionHandler: function actionHandler() {
        props.tableHandlers.editActionHandler(props.item, props.index, props.tableModel);
      },
      actionIcon: "nc-icon nc-ruler-pencil",
      actionText: "Edit"
    }));
  }

  if (props.tableOptions.showDeleteAction) {
    actionButtons.push( /*#__PURE__*/React__default.createElement(TableActionButton, {
      key: "delete-action",
      id: "deleteAction-" + index,
      color: "danger",
      actionHandler: function actionHandler() {
        props.tableHandlers.deleteActionHandler(props.item, props.index, props.tableModel);
      },
      actionIcon: "fa fa-times",
      actionText: "Delete"
    }));
  }

  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, actionButtons);
}

function ReactTableBasicContentRenderer(_ref) {
  var tableOptions = _ref.tableOptions,
      tableHandlers = _ref.tableHandlers,
      getTableBodyProps = _ref.getTableBodyProps,
      page = _ref.page,
      prepareRow = _ref.prepareRow;
  var styleClass = "";

  var cellMapper = function cellMapper(cell) {
    var tdOnClick = null;

    if (tableOptions.clickableTable) {
      styleClass = "clickable";

      tdOnClick = function tdOnClick() {
        return tableHandlers.tableRowClickHandler(cell.row.original, cell.row.index, cell.column.id);
      };
    }

    return /*#__PURE__*/React__default.createElement("td", _extends({
      className: styleClass
    }, cell.getCellProps(), {
      onClick: tdOnClick
    }), cell.render("Cell"));
  };

  var rowMaper = function rowMaper(row) {
    prepareRow(row);
    var cells = row.cells.map(cellMapper);

    if (tableOptions.showActions) {
      cells.push( /*#__PURE__*/React__default.createElement("td", {
        key: "action-buttons-" + row.index
      }, /*#__PURE__*/React__default.createElement(TableActionButtons, {
        index: row.index,
        item: row.original,
        tableModel: null,
        tableOptions: tableOptions,
        tableHandlers: tableHandlers
      })));
    }

    return /*#__PURE__*/React__default.createElement("tr", row.getRowProps(), cells);
  };

  var tableRows = page.map(rowMaper);
  return /*#__PURE__*/React__default.createElement("tbody", getTableBodyProps(), tableRows);
}

var DISPLAY_ADDITIONAL_PAGES_COUNT = 3;
var MAX_DISPLAY_PAGES_COUNT = DISPLAY_ADDITIONAL_PAGES_COUNT * 2 + 1;
var PAGE_SIZE_OPTIONS = [5, 10, 20, 50, 100];

function DefaultPaginationRenderer(_ref) {
  var pageCount = _ref.pageCount,
      pageIndex = _ref.pageIndex,
      canPreviousPage = _ref.canPreviousPage,
      canNextPage = _ref.canNextPage,
      setPageSize = _ref.setPageSize,
      gotoPage = _ref.gotoPage,
      nextPage = _ref.nextPage,
      previousPage = _ref.previousPage,
      defaultPageSize = _ref.defaultPageSize;

  var getPagesToDisplay = function getPagesToDisplay() {
    var pages = [];
    var page = pageIndex > DISPLAY_ADDITIONAL_PAGES_COUNT ? pageIndex - DISPLAY_ADDITIONAL_PAGES_COUNT : 0;

    while (page < pageCount && pages.length < MAX_DISPLAY_PAGES_COUNT) {
      pages.push(page);
      ++page;
    }

    return pages;
  };

  var gotoFirstPage = function gotoFirstPage() {
    return gotoPage(0);
  };

  var gotoLastPage = function gotoLastPage() {
    return gotoPage(pageCount - 1);
  };

  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement(reactstrap.Row, null, /*#__PURE__*/React__default.createElement(reactstrap.Col, {
    xs: "auto",
    className: "ml-auto"
  }, /*#__PURE__*/React__default.createElement(reactstrap.Pagination, null, /*#__PURE__*/React__default.createElement(reactstrap.PaginationItem, {
    disabled: !canPreviousPage
  }, /*#__PURE__*/React__default.createElement(reactstrap.PaginationLink, {
    first: true,
    onClick: gotoFirstPage
  })), /*#__PURE__*/React__default.createElement(reactstrap.PaginationItem, {
    disabled: !canPreviousPage
  }, /*#__PURE__*/React__default.createElement(reactstrap.PaginationLink, {
    previous: true,
    onClick: previousPage
  })), getPagesToDisplay().map(function (pageNumber) {
    return /*#__PURE__*/React__default.createElement(reactstrap.PaginationItem, {
      active: pageNumber === pageIndex,
      key: pageNumber
    }, /*#__PURE__*/React__default.createElement(reactstrap.PaginationLink, {
      onClick: function onClick() {
        return gotoPage(pageNumber);
      }
    }, pageNumber + 1));
  }), /*#__PURE__*/React__default.createElement(reactstrap.PaginationItem, {
    disabled: !canNextPage
  }, /*#__PURE__*/React__default.createElement(reactstrap.PaginationLink, {
    next: true,
    onClick: nextPage
  })), /*#__PURE__*/React__default.createElement(reactstrap.PaginationItem, {
    disabled: !canNextPage
  }, /*#__PURE__*/React__default.createElement(reactstrap.PaginationLink, {
    last: true,
    onClick: gotoLastPage
  })))), /*#__PURE__*/React__default.createElement(reactstrap.Col, {
    xs: "auto",
    className: "mr-auto"
  }, /*#__PURE__*/React__default.createElement(reactstrap.Input, {
    type: "select",
    defaultValue: defaultPageSize,
    onChange: function onChange(event) {
      setPageSize(event.target.value);
    }
  }, PAGE_SIZE_OPTIONS.map(function (pageSize) {
    return /*#__PURE__*/React__default.createElement("option", {
      value: pageSize,
      key: pageSize
    }, pageSize, " / page");
  })))));
}

function CraftsmenRestTable(_ref) {
  var columns = _ref.columns,
      tableOptions = _ref.tableOptions,
      tableHandlers = _ref.tableHandlers,
      fetchData = _ref.fetchData,
      updateTrigger = _ref.updateTrigger,
      onCursorDataStateChange = _ref.onCursorDataStateChange,
      _ref$initialCursorDat = _ref.initialCursorDataState,
      initialCursorDataState = _ref$initialCursorDat === void 0 ? {
    pageIndex: 0,
    pageSize: 10,
    sortBy: [{}]
  } : _ref$initialCursorDat;

  var _useState = React.useState([]),
      data = _useState[0],
      setData = _useState[1];

  var _useState2 = React.useState(-1),
      controlledPageCount = _useState2[0],
      setControlledPageCount = _useState2[1];

  var _useState3 = React.useState(false),
      isLoading = _useState3[0],
      setIsLoading = _useState3[1];

  var _useTable = reactTable.useTable({
    columns: columns,
    data: data,
    initialState: _extends({
      pageIndex: 0
    }, initialCursorDataState),
    manualPagination: true,
    pageCount: controlledPageCount,
    manualSortBy: true,
    disableSortBy: !tableOptions.columnSorting
  }, reactTable.useSortBy, reactTable.usePagination),
      getTableProps = _useTable.getTableProps,
      getTableBodyProps = _useTable.getTableBodyProps,
      headerGroups = _useTable.headerGroups,
      prepareRow = _useTable.prepareRow,
      page = _useTable.page,
      canPreviousPage = _useTable.canPreviousPage,
      canNextPage = _useTable.canNextPage,
      pageOptions = _useTable.pageOptions,
      pageCount = _useTable.pageCount,
      gotoPage = _useTable.gotoPage,
      nextPage = _useTable.nextPage,
      previousPage = _useTable.previousPage,
      setPageSize = _useTable.setPageSize,
      _useTable$state = _useTable.state,
      pageIndex = _useTable$state.pageIndex,
      pageSize = _useTable$state.pageSize,
      sortBy = _useTable$state.sortBy;

  var fetchIdRef = React__default.useRef(0);
  var fetchDataCallback = React__default.useCallback(function (pageIndex, pageSize) {
    var fetchId = ++fetchIdRef.current;
    setIsLoading(true);
    var fetchOptions = {};

    if (sortBy.length > 0) {
      fetchOptions.sort = sortBy;
    }

    fetchData(pageIndex, pageSize, fetchOptions).then(function (result) {
      try {
        if (fetchId === fetchIdRef.current) {
          setIsLoading(false);
          setControlledPageCount(result.totalPages);
          setData(result.content);
          setPageSize(result.size);
          onCursorDataStateChange(pageIndex, pageSize, sortBy);
        }
      } catch (error) {
        console.error("Error when fetching data", error);
      }
    });
  }, [fetchData, setPageSize, sortBy]);
  React__default.useEffect(function () {
    fetchDataCallback(pageIndex, pageSize);
  }, [fetchDataCallback, pageIndex, pageSize, updateTrigger]);
  var HeaderRenderer = getHeaderRenderer();
  var ContentRenderer = getContentRenderer();
  var PaginationRenderer = getPaginationRenderer();
  var finalTableOptions = getTableOptions(tableOptions);
  var finalTableHandlers = getTableHandler(tableHandlers);
  return /*#__PURE__*/React__default.createElement("div", {
    className: "position-relative"
  }, isLoading ? /*#__PURE__*/React__default.createElement("div", {
    className: "position-absolute w-100 h-100 d-flex",
    style: {
      zIndex: 2
    }
  }, /*#__PURE__*/React__default.createElement("h2", {
    className: "m-auto"
  }, "Loading ", /*#__PURE__*/React__default.createElement(reactstrap.Spinner, {
    color: "primary"
  }))) : null, /*#__PURE__*/React__default.createElement("div", {
    style: {
      filter: isLoading ? "blur(2px)" : ""
    }
  }, /*#__PURE__*/React__default.createElement(reactstrap.Table, _extends({
    hover: tableOptions.clickableTable
  }, getTableProps()), /*#__PURE__*/React__default.createElement(HeaderRenderer, {
    tableOptions: finalTableOptions,
    tableHandlers: finalTableHandlers,
    headerGroups: headerGroups
  }), /*#__PURE__*/React__default.createElement(ContentRenderer, {
    tableOptions: finalTableOptions,
    tableHandlers: finalTableHandlers,
    getTableBodyProps: getTableBodyProps,
    page: page,
    prepareRow: prepareRow
  })), /*#__PURE__*/React__default.createElement(PaginationRenderer, {
    pageCount: pageCount,
    pageOptions: pageOptions,
    pageIndex: pageIndex,
    canPreviousPage: canPreviousPage,
    canNextPage: canNextPage,
    setPageSize: setPageSize,
    defaultPageSize: pageSize,
    gotoPage: gotoPage,
    nextPage: nextPage,
    previousPage: previousPage
  })));
}
CraftsmenRestTable.defaultProps = {
  updateTrigger: 0
};

var getHeaderRenderer = function getHeaderRenderer(props) {
  return ReactTableBasicHeaderRenderer;
};

var getContentRenderer = function getContentRenderer(props) {
  return ReactTableBasicContentRenderer;
};

var getPaginationRenderer = function getPaginationRenderer(props) {
  return DefaultPaginationRenderer;
};

var getTableOptions = function getTableOptions(tableOptions) {
  var finalTableOptions = new TableOptions();

  if (tableOptions) {
    finalTableOptions.putTableOptions(tableOptions);
  }

  return finalTableOptions;
};

var getTableHandler = function getTableHandler(tableHandlers) {
  var finalTableHandlers = new TableHandlers();

  if (tableHandlers) {
    finalTableHandlers.putTableHandlers(tableHandlers);
  }

  return finalTableHandlers;
};

exports.CraftsmenRestTable = CraftsmenRestTable;
exports.TableHandlers = TableHandlers;
exports.TableOptions = TableOptions;
exports.default = CraftsmenRestTable;
//# sourceMappingURL=index.js.map
