import React from "react";
import { UncontrolledTooltip, Button } from "reactstrap";

function TableActionButton({
  id,
  actionText,
  color,
  actionHandler,
  actionIcon,
}) {
  return (
    <>
      <UncontrolledTooltip placement="top" target={id}>
        {actionText}
      </UncontrolledTooltip>

      <Button
        id={id}
        outline
        color={color}
        size="sm"
        onClick={actionHandler}
        className="btn-icon btn-link btn-round"
      >
        <i className={actionIcon} />
      </Button>
    </>
  );
}

export default TableActionButton;
