import React from "react";
import TableActionButtons from "./TableActionButtons";

function ReactTableBasicContentRenderer({
  tableOptions,
  tableHandlers,
  getTableBodyProps,
  page,
  prepareRow,
}) {
  let styleClass = "";

  const cellMapper = (cell) => {
    let tdOnClick = null;
    if (tableOptions.clickableTable) {
      styleClass = "clickable";
      tdOnClick = () =>
        tableHandlers.tableRowClickHandler(
          cell.row.original,
          cell.row.index,
          cell.column.id
        );
    }
    return (
      <td className={styleClass} {...cell.getCellProps()} onClick={tdOnClick}>
        {cell.render("Cell")}
      </td>
    );
  };

  const rowMaper = (row) => {
    prepareRow(row);

    const cells = row.cells.map(cellMapper);

    if (tableOptions.showActions) {
      cells.push(
        <td key={"action-buttons-" + row.index}>
          <TableActionButtons
            index={row.index}
            item={row.original}
            tableModel={null}
            tableOptions={tableOptions}
            tableHandlers={tableHandlers}
          />
        </td>
      );
    }

    return <tr {...row.getRowProps()}>{cells}</tr>;
  };

  const tableRows = page.map(rowMaper);

  return <tbody {...getTableBodyProps()}>{tableRows}</tbody>;
}

export default ReactTableBasicContentRenderer;
