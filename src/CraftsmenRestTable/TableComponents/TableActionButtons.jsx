import React from "react";
import TableActionButton from "./TableActionButton";

function TableActionButtons(props) {
  const actionButtons = [];

  const index = props.index;

  if (props.tableOptions.showViewAction) {
    actionButtons.push(
      <TableActionButton
        key="view-action"
        id={"viewAction-" + index}
        color="info"
        actionHandler={() => {
          props.tableHandlers.viewActionHandler(
            props.item,
            props.index,
            props.tableModel
          );
        }}
        actionIcon="nc-icon nc-alert-circle-i"
        actionText="View"
      />
    );
  }

  if (props.tableOptions.showEditAction) {
    actionButtons.push(
      <TableActionButton
        key="edit-action"
        id={"editAction-" + index}
        color="primary"
        actionHandler={() => {
          props.tableHandlers.editActionHandler(
            props.item,
            props.index,
            props.tableModel
          );
        }}
        actionIcon="nc-icon nc-ruler-pencil"
        actionText="Edit"
      />
    );
  }

  if (props.tableOptions.showDeleteAction) {
    actionButtons.push(
      <TableActionButton
        key="delete-action"
        id={"deleteAction-" + index}
        color="danger"
        actionHandler={() => {
          props.tableHandlers.deleteActionHandler(
            props.item,
            props.index,
            props.tableModel
          );
        }}
        actionIcon="fa fa-times"
        actionText="Delete"
      />
    );
  }

  return <>{actionButtons}</>;
}

export default TableActionButtons;
