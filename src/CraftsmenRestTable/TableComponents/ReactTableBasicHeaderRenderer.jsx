import React from "react";

function ReactTableBasicHeaderRenderer({
  headerGroups,
  tableOptions,
  tableHandlers
}) {
  let actionHeaders = null;
  if (tableOptions.showActions) {
    actionHeaders = <th key="action-buttons-header">Actions</th>;
  }

  const columnMaper = column => {
    return (
      <th {...column.getHeaderProps(column.getSortByToggleProps)}>
        {column.render("Header")}
        <span>
          {column.isSorted ? (column.isSortedDesc ? " 🔽" : " 🔼") : ""}
        </span>
      </th>
    );
  };

  const headerGroupsMaper = headerGroup => {
    const columns = headerGroup.headers.map(columnMaper);

    columns.push(actionHeaders);

    return <tr {...headerGroup.getHeaderGroupProps()}>{columns}</tr>;
  };

  return <thead>{headerGroups.map(headerGroupsMaper)}</thead>;
}

export default ReactTableBasicHeaderRenderer;
