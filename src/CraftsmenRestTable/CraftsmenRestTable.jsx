import React, { useState } from "react";

// reactstrap components
import { Spinner, Table } from "reactstrap";

import TableHandlers from "./TableHandlers";
import TableOptions from "./TableOptions";

import ReactTableBasicHeaderRenderer from "./TableComponents/ReactTableBasicHeaderRenderer";
import ReactTableBasicContentRenderer from "./TableComponents/ReactTableBasicContentRenderer";

import DefaultPaginationRenderer from "./PaginationRenderers/DefaultPaginationRenderer";

import { useTable, usePagination, useSortBy } from "react-table";

export function CraftsmenRestTable({
  columns,
  tableOptions,
  tableHandlers,
  fetchData,
  updateTrigger, // Used for refetching data (updating)
  onCursorDataStateChange,
  initialCursorDataState = { pageIndex: 0, pageSize: 10, sortBy: [{}] },
}) {
  const [data, setData] = useState([]);
  const [controlledPageCount, setControlledPageCount] = useState(-1);
  const [isLoading, setIsLoading] = useState(false);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    // Get the state from the instance
    state: { pageIndex, pageSize, sortBy },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, ...initialCursorDataState }, // Pass our hoisted table state
      manualPagination: true, // Tell the usePagination
      // hook that we'll handle our own data fetching
      // This means we'll also have to provide our own
      // pageCount.
      pageCount: controlledPageCount,
      manualSortBy: true,
      disableSortBy: !tableOptions.columnSorting,
    },
    useSortBy,
    usePagination
  );

  const fetchIdRef = React.useRef(0);

  const fetchDataCallback = React.useCallback(
    (pageIndex, pageSize) => {
      const fetchId = ++fetchIdRef.current;

      setIsLoading(true);

      let fetchOptions = {};
      if (sortBy.length > 0) {
        fetchOptions.sort = sortBy;
      }

      fetchData(pageIndex, pageSize, fetchOptions).then((result) => {
        try {
          if (fetchId === fetchIdRef.current) {
            setIsLoading(false);

            setControlledPageCount(result.totalPages);
            setData(result.content);
            setPageSize(result.size);
            onCursorDataStateChange(pageIndex, pageSize, sortBy);
          }
        } catch (error) {
          console.error("Error when fetching data", error);
        }
      });
    },
    [fetchData, setPageSize, sortBy]
  );

  React.useEffect(() => {
    // Fetching for pagination
    fetchDataCallback(pageIndex, pageSize);
    // Refetching occurs also when updateTrigger has changed
  }, [fetchDataCallback, pageIndex, pageSize, updateTrigger]);

  const HeaderRenderer = getHeaderRenderer();
  const ContentRenderer = getContentRenderer();
  const PaginationRenderer = getPaginationRenderer();

  const finalTableOptions = getTableOptions(tableOptions);
  const finalTableHandlers = getTableHandler(tableHandlers);

  return (
    <div className="position-relative">
      {isLoading ? (
        <div
          className="position-absolute w-100 h-100 d-flex"
          style={{ zIndex: 2 }}
        >
          <h2 className="m-auto">
            Loading <Spinner color="primary"></Spinner>
          </h2>
        </div>
      ) : null}
      <div style={{ filter: isLoading ? "blur(2px)" : "" }}>
        <Table hover={tableOptions.clickableTable} {...getTableProps()}>
          <HeaderRenderer
            tableOptions={finalTableOptions}
            tableHandlers={finalTableHandlers}
            headerGroups={headerGroups}
          />
          <ContentRenderer
            tableOptions={finalTableOptions}
            tableHandlers={finalTableHandlers}
            getTableBodyProps={getTableBodyProps}
            page={page}
            prepareRow={prepareRow}
          />
        </Table>
        <PaginationRenderer
          pageCount={pageCount}
          pageOptions={pageOptions}
          pageIndex={pageIndex}
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          setPageSize={setPageSize}
          defaultPageSize={pageSize}
          gotoPage={gotoPage}
          nextPage={nextPage}
          previousPage={previousPage}
        />
      </div>
    </div>
  );
}

// Set default value of updateTrigger
CraftsmenRestTable.defaultProps = {
  updateTrigger: 0,
};

const getHeaderRenderer = (props) => {
  return ReactTableBasicHeaderRenderer;
};

const getContentRenderer = (props) => {
  return ReactTableBasicContentRenderer;
};

const getPaginationRenderer = (props) => {
  return DefaultPaginationRenderer;
};

const getTableOptions = (tableOptions) => {
  const finalTableOptions = new TableOptions();

  if (tableOptions) {
    finalTableOptions.putTableOptions(tableOptions);
  }

  return finalTableOptions;
};

const getTableHandler = (tableHandlers) => {
  const finalTableHandlers = new TableHandlers();

  if (tableHandlers) {
    finalTableHandlers.putTableHandlers(tableHandlers);
  }

  return finalTableHandlers;
};

export default CraftsmenRestTable;
