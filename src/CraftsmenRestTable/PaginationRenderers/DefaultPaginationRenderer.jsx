import React from "react";
import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Input,
  Row,
  Col,
} from "reactstrap";

const DISPLAY_ADDITIONAL_PAGES_COUNT = 3;
const MAX_DISPLAY_PAGES_COUNT = DISPLAY_ADDITIONAL_PAGES_COUNT * 2 + 1;
const PAGE_SIZE_OPTIONS = [5, 10, 20, 50, 100];

function DefaultPaginationRenderer({
  pageCount,
  pageIndex,
  canPreviousPage,
  canNextPage,
  setPageSize,
  gotoPage,
  nextPage,
  previousPage,
  defaultPageSize,
}) {
  const getPagesToDisplay = () => {
    const pages = [];
    let page =
      pageIndex > DISPLAY_ADDITIONAL_PAGES_COUNT
        ? pageIndex - DISPLAY_ADDITIONAL_PAGES_COUNT
        : 0;

    while (page < pageCount && pages.length < MAX_DISPLAY_PAGES_COUNT) {
      pages.push(page);
      ++page;
    }

    return pages;
  };

  const gotoFirstPage = () => gotoPage(0);
  const gotoLastPage = () => gotoPage(pageCount - 1);

  return (
    <>
      <Row>
        <Col xs="auto" className="ml-auto">
          <Pagination>
            <PaginationItem disabled={!canPreviousPage}>
              <PaginationLink first onClick={gotoFirstPage} />
            </PaginationItem>

            <PaginationItem disabled={!canPreviousPage}>
              <PaginationLink previous onClick={previousPage} />
            </PaginationItem>

            {getPagesToDisplay().map((pageNumber) => (
              <PaginationItem
                active={pageNumber === pageIndex}
                key={pageNumber}
              >
                <PaginationLink onClick={() => gotoPage(pageNumber)}>
                  {pageNumber + 1}
                </PaginationLink>
              </PaginationItem>
            ))}

            <PaginationItem disabled={!canNextPage}>
              <PaginationLink next onClick={nextPage} />
            </PaginationItem>

            <PaginationItem disabled={!canNextPage}>
              <PaginationLink last onClick={gotoLastPage} />
            </PaginationItem>
          </Pagination>
        </Col>

        <Col xs="auto" className="mr-auto">
          <Input
            type="select"
            defaultValue={defaultPageSize}
            onChange={(event) => {
              setPageSize(event.target.value);
            }}
          >
            {PAGE_SIZE_OPTIONS.map((pageSize) => (
              <option value={pageSize} key={pageSize}>
                {pageSize} / page
              </option>
            ))}
          </Input>
        </Col>
      </Row>
    </>
  );
}

export default DefaultPaginationRenderer;
