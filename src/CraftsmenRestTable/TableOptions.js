export class TableOptions {
  showViewAction = false;
  showEditAction = false;
  showDeleteAction = false;
  clickableTable = true;
  columnSorting = false;

  get showActions() {
    return this.showViewAction || this.showEditAction || this.showDeleteAction;
  }

  putTableOptions = (tableOptions) => {
    for (var key in tableOptions) {
      this[key] = tableOptions[key];
    }
  };
}

export default TableOptions;
