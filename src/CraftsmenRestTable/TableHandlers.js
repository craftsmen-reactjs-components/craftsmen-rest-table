export class TableHandlers {
  defaultDummyHandler = () => {
    console.warn("No TableHandler");
  };

  viewActionHandler = this.defaultDummyHandler; // (item, index)
  editActionHandler = this.defaultDummyHandler; // (item, index),
  deleteActionHandler = this.defaultDummyHandler; // (item, index),
  tableRowClickHandler = this.defaultDummyHandler; // (item, rowIndex, columnId),

  putTableHandlers = (tableHandlers) => {
    for (var key in tableHandlers) {
      this[key] = tableHandlers[key];
    }
  };
}

export default TableHandlers;
