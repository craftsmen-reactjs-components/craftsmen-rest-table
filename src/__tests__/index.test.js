import { CraftsmenRestTable } from "..";
import React from "react";
import {
  render,
  screen,
  // waitForElementToBeRemoved,
} from "@testing-library/react";
import "@testing-library/jest-dom";

const tableColumns = [
  {
    Header: "#",
    accessor: "id",
  },
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Description",
    accessor: "description",
  },
  {
    Header: "# of datasets",
    accessor: "numberOfDataSets",
  },
];

describe("CraftsmenRestTable table structure test with actions", () => {
  it("is truthy", () => {
    expect(CraftsmenRestTable).toBeTruthy();
  });

  const craftsmenRestTableRender = () =>
    render(
      <CraftsmenRestTable
        columns={tableColumns}
        fetchData={() => {
          return new Promise((resolve, reject) => {});
        }}
        tableOptions={{
          showEditAction: true,
          showViewAction: true,
          showDeleteAction: true,
          clickableTable: true,
        }}
        tableHandlers={{
          viewActionHandler: () => {},
          editActionHandler: () => {},
          deleteActionHandler: () => {},
          tableRowClickHandler: () => {},
        }}
      />
    );
  test("Test table headers", () => {
    craftsmenRestTableRender();

    tableColumns.forEach((column) =>
      expect(screen.getByText(column.Header)).toBeInTheDocument()
    );

    expect(screen.getByText("Actions")).toBeInTheDocument();

    expect(screen.getAllByRole("columnheader").length).toBe(
      tableColumns.length + 1
    );
  });

  test("Test loading feedback", async () => {
    craftsmenRestTableRender();

    expect(screen.getByRole("status")).toBeInTheDocument();

    // await waitForElementToBeRemoved(() => screen.queryByRole("status"));
  });
});
