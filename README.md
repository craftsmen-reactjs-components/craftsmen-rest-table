# Craftsmen Rest Table

> Generic REST table with automatic data handling, paging and default actions
> Inherited from React Table, a hook for building lightweight, fast and extendable datagrids for React & NextJS
> Based on Reactstrap library

[![NPM](https://img.shields.io/npm/v/craftsmen-rest-table.svg)](https://www.npmjs.com/package/craftsmen-rest-table) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save craftsmen-rest-table
```

## Available features

- Generic and usable anywhere
- Column Sorting
- Column Actions (Delete, Edit, View)
- Pagination included
- Accessible to certain page/size with URL

## Simple Usage

```jsx
import React, { Component } from "react";

import CraftsmenRestTable from "craftsmen-rest-table";
import "craftsmen-rest-table/dist/index.css";

const Example = () => {
  return (
    <CraftsmenRestTable
      columns={tableColumns}
      fetchData={restService.getAll}
      tableOptions={{
        showEditAction: true,
        clickableTable: true,
        columnSorting: false
      }}
    />;
  );

}
```

## Complete Usage

```jsx
const Example = () => {
  return (
    <CraftsmenRestTable
      columns={tableColumns}
      fetchData={DataSetCollectionsService.getAll}
      tableOptions={{
        showEditAction: true,
        showViewAction: true,
        showDeleteAction: true,
        clickableTable: true,
        columnSorting: true,
      }}
      tableHandlers={{
        viewActionHandler: templateViewActionHandler,
        editActionHandler: templateEditActionHandler,
        deleteActionHandler: templateDeleteActionHandler,
        tableRowClickHandler: templateRowClicked,
      }}
      onCursorDataStateChange={(page, size, sortOptions) =>
        console.log(page, size, sortOptions)
      }
      initialCursorDataState={{
        pageIndex: 0,
        pageSize: 5,
        sortBy: [{}],
      }}
    />
  );
};
```

## Note

- For more details of the implmentation, please check **example** project in this repository

## Author

SaaS Craftsmen Team

## License

EULA © [](https://github.com/)
