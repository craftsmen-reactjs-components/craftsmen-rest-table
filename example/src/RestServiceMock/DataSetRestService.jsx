import { getPageableJsonArray } from "./DataSetCollectionRestService";

export var dataSets = [
  {
    id: 1,
    name: "Construction company Names",
    dataSetCollection: 1,
    entityVersion: 0,
    createdOn: "2020-05-22T08:46:35",
    updatedOn: "2020-06-22T18:08:03"
  },
  {
    id: 2,
    name: "Construction company urls",
    dataSetCollection: 1,
    entityVersion: 0,
    createdOn: "2020-05-22T08:46:35",
    updatedOn: "2020-06-22T18:08:03"
  },
  {
    id: 3,
    name: "some data set",
    dataSetCollection: 3,
    entityVersion: 0,
    createdOn: "2020-05-22T08:46:35",
    updatedOn: "2020-06-22T18:08:03"
  }
];

var dataSetsContent = {
  2: {
    meta: {
      type: "data-set-content",
      syntaxVersion: 1,
      editorVersion: "1.0.0"
    },
    content:
      "http://example/com/url1\nhttp://example/com/url2\nhttp://example/com/url3\n"
  }
};

export class DataSetRestService {
  constructor(endpointUrl) {
    this.endpointUrl = endpointUrl;
  }

  get = id => {
    return new Promise((resolve, reject) => {
      let returnValue = dataSets.find(element => element.id === id);
      resolve(returnValue);
    });
  };

  getAll = (page = 0, size = 20) => {
    return new Promise((resolve, reject) => {
      let returnValue = getPageableJsonArray(page, size, dataSets);
      resolve(returnValue);
    });
  };

  delete = id => {
    return new Promise((resolve, reject) => {
      let deleteIndex = dataSets.findIndex(element => element.id === id);

      if (deleteIndex === -1) {
        reject("No element at id: " + id);
      } else {
        let deletedElement = dataSets[deleteIndex];
        dataSets.splice(deleteIndex, 1);
        resolve(deletedElement);
      }
    });
  };

  put = editedData => {
    return new Promise((resolve, reject) => {
      let putIndex = dataSets.findIndex(
        element => element.id === editedData.id
      );

      if (putIndex === -1) {
        reject("No element at id: " + editedData.id);
      } else {
        let nextEntityVersion = dataSets[putIndex].entityVersion + 1;
        dataSets[putIndex] = {
          ...dataSets[putIndex],
          ...editedData,
          updatedOn: new Date(Date.now()).toISOString(),
          entityVersion: nextEntityVersion
        };
        resolve(dataSets[putIndex]);
      }
    });
  };

  getDataSetContent = id => {
    return new Promise((resolve, reject) => {
      let content = dataSetsContent[id] || "";
      resolve(content);
    });
  };

  postDataSetContent = (id, content) => {
    return new Promise((resolve, reject) => {
      dataSetsContent[id] = { ...content };
      resolve(dataSetsContent[id]);
    });
  };
}

export default DataSetRestService;
