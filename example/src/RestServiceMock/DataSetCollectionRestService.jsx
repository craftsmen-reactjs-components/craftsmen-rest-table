import { dataSets } from "./DataSetRestService";

var dataSetCollections = [
  {
    id: 1,
    name: "Construction company",
    description: "Datasets for any kind of construction company",
    numberOfDataSets: 6,
    createdOn: "2020-05-22T08:46:35",
    updatedOn: "2020-06-22T18:08:03",
  },
  {
    id: 2,
    name: "data set collection 2",
    description: "Description of collection 2",
    numberOfDataSets: 0,
    createdOn: "2020-05-22T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 3,
    name: "data set collection 3",
    description: "Description of collection 3",
    numberOfDataSets: 4,
    createdOn: "2020-05-22T08:46:35",
    updatedOn: "2020-06-22T18:08:03",
  },
  {
    id: 4,
    name: "data set collection 4",
    description: "Description of collection 4",
    numberOfDataSets: 0,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-24T18:08:03",
  },
  {
    id: 6,
    name: "data set collection 6",
    description: "Description of collection 6",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 7,
    name: "data set collection 7",
    description: "Description of collection 7",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 8,
    name: "data set collection 8",
    description: "Description of collection 8",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 9,
    name: "data set collection 9",
    description: "Description of collection 9",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 10,
    name: "data set collection 10",
    description: "Description of collection 10",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 11,
    name: "data set collection 11",
    description: "Description of collection 11",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 12,
    name: "data set collection 12",
    description: "Description of collection 12",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 13,
    name: "data set collection 13",
    description: "Description of collection 13",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 14,
    name: "data set collection 14",
    description: "Description of collection 14",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 15,
    name: "data set collection 15",
    description: "Description of collection 15",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 16,
    name: "data set collection 16",
    description: "Description of collection 16",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 17,
    name: "data set collection 17",
    description: "Description of collection 17",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 18,
    name: "data set collection 18",
    description: "Description of collection 18",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 19,
    name: "data set collection 19",
    description: "Description of collection 19",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 20,
    name: "data set collection 20",
    description: "Description of collection 20",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 21,
    name: "data set collection 21",
    description: "Description of collection 21",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 22,
    name: "data set collection 22",
    description: "Description of collection 22",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
  {
    id: 23,
    name: "data set collection 23",
    description: "Description of collection 23",
    numberOfDataSets: 10,
    createdOn: "2020-05-24T08:46:35",
    updatedOn: "2020-06-28T18:08:03",
  },
];

function sortDataFunctionGenerator(sortingOptions) {
  return function (a, b) {
    let retVal = 0;
    sortingOptions.some((opt) => {
      if (a[opt.id] > b[opt.id]) {
        retVal = opt.desc ? -1 : 1;
        return true;
      } else if (a[opt.id] < b[opt.id]) {
        retVal = opt.desc ? 1 : -1;
        return true;
      } else return false;
    });
    return retVal;
  };
}

export function sortAndFilterJsonArray(data, options) {
  if (options.sort !== undefined) {
    return data.sort(sortDataFunctionGenerator(options.sort));
  } else {
    return data;
  }
}

export function getPageableJsonArray(page, size, targetArray) {
  const totalPages = Math.ceil(targetArray.length / size);
  const contentArray = targetArray.slice(page * size, page * size + size);
  return {
    content: contentArray,
    pageable: {
      sort: {
        sorted: false,
        unsorted: true,
        empty: true,
      },
      offset: page * size,
      pageNumber: totalPages,
      pageSize: size,
      paged: true,
      unpaged: false,
    },
    last: page + 1 >= totalPages,
    totalPages: totalPages,
    totalElements: targetArray.length,
    first: page === 0,
    sort: {
      sorted: false,
      unsorted: true,
      empty: true,
    },
    size: size,
    number: page,
    numberOfElements: contentArray.length,
    empty: contentArray.length === 0,
  };
}

export class DataSetCollectionRestService {
  constructor(endpointUrl) {
    this.endpointUrl = endpointUrl;
  }

  get = (id) => {
    return new Promise((resolve, reject) => {
      let returnValue = dataSetCollections.find((element) => element.id === id);
      resolve(returnValue);
    });
  };

  getAll = (page = 0, size = 20, options) => {
    return new Promise((resolve, reject) => {
      let sortedData = sortAndFilterJsonArray(dataSetCollections, options);
      let returnValue = getPageableJsonArray(page, size, sortedData);
      setTimeout(() => resolve(returnValue), 100);
    });
  };

  delete = (id) => {
    return new Promise((resolve, reject) => {
      let deleteIndex = dataSetCollections.findIndex(
        (element) => element.id === id
      );

      if (deleteIndex === -1) {
        reject("No element at id: " + id);
      } else {
        let deletedElement = dataSetCollections[deleteIndex];
        dataSetCollections.splice(deleteIndex, 1);
        resolve(deletedElement);
      }
    });
  };

  put = (editedData) => {
    return new Promise((resolve, reject) => {
      let putIndex = dataSetCollections.findIndex(
        (element) => element.id === editedData.id
      );

      if (putIndex === -1) {
        reject("No element at id: " + editedData.id);
      } else {
        dataSetCollections[putIndex] = {
          ...dataSetCollections[putIndex],
          ...editedData,
          updatedOn: new Date(Date.now()).toISOString(),
        };
        resolve(dataSetCollections[putIndex]);
      }
    });
  };

  post = (postData) => {
    return new Promise((resolve, reject) => {
      let nextId = dataSetCollections[dataSetCollections.length - 1].id + 1;
      let currentDate = new Date(Date.now()).toISOString();

      dataSetCollections.push({
        ...postData,
        id: nextId,
        numberOfDataSets: 0,
        createdOn: currentDate,
        updatedOn: currentDate,
      });
      resolve(dataSetCollections[dataSetCollections.length - 1]);
    });
  };

  getDataSetsFromCollection = (collectionId, page = 0, size = 20) => {
    return new Promise((resolve, reject) => {
      let dataSetsFromCollection = dataSets.filter(
        (dataSet) => dataSet.dataSetCollection === collectionId
      );
      let returnValue = getPageableJsonArray(
        page,
        size,
        dataSetsFromCollection
      );
      resolve(returnValue);
    });
  };

  postDataSetToCollection = (collectionId, dataSetData) => {
    return new Promise((resolve, reject) => {
      let nextId = dataSets[dataSets.length - 1].id + 1;
      let currentDate = new Date(Date.now()).toISOString();

      dataSets.push({
        ...dataSetData,
        id: nextId,
        dataSetCollection: collectionId,
        entityVersion: 0,
        createdOn: currentDate,
        updatedOn: currentDate,
      });
      resolve(dataSets[dataSets.length - 1]);
    });
  };
}

const dataSetCollectionRestService = new DataSetCollectionRestService();

export default dataSetCollectionRestService;
