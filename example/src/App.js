import React from "react";
import "bootstrap/dist/css/bootstrap.css";

//import { ExampleComponent } from 'craftsmen-rest-table'
import CraftsmenRestTable from "craftsmen-rest-table";
import DataSetCollectionsService from "./RestServiceMock/DataSetCollectionRestService";
import { createBrowserHistory } from "history";

import "./index.css";

const tableColumns = [
  {
    Header: "#",
    accessor: "id",
  },
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Description",
    accessor: "description",
  },
  {
    Header: "# of datasets",
    accessor: "numberOfDataSets",
  },
];

const App = () => {
  const history = createBrowserHistory();
  const setCurrentPageSize = (page, size) => {
    history.push(`page=${page}&size=${size}`);
  };
  return (
    <>
      <CraftsmenRestTable
        columns={tableColumns}
        fetchData={DataSetCollectionsService.getAll}
        tableOptions={{
          showEditAction: true,
          showViewAction: true,
          showDeleteAction: true,
          clickableTable: true,
          columnSorting: true,
        }}
        tableHandlers={{
          viewActionHandler: templateViewActionHandler,
          editActionHandler: templateEditActionHandler,
          deleteActionHandler: templateDeleteActionHandler,
          tableRowClickHandler: templateRowClicked,
        }}
        onCursorDataStateChange={(page, size, sortOptions) =>
          setCurrentPageSize(page, size, sortOptions)
        }
        initialCursorDataState={{
          pageIndex: 0,
          pageSize: 5,
          sortBy: [{}],
        }}
      />
    </>
  );
};

const templateViewActionHandler = (item, index, tableModel) => {
  console.log("Viewing item: ", item);
};

const templateEditActionHandler = (item, index, tableModel) => {
  console.log("editing item: ", item);
};

const templateDeleteActionHandler = (item, index, tableModel) => {
  console.log("Deleting item: ", item);
};

const templateRowClicked = (item, rowIndex, columnIndex, tableModel) => {
  console.log("Item row clicked: ", item);
};

export default App;
